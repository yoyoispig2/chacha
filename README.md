# chacha

### Features:
Currently there are two models run together in the server, later we will make it into one model.

a. Current supported label (based on pretrained coco model):
- Car / 小轎車 / 汽車 / 計程車 (taxi)
- Bus / 公車
- Fire hydrant / 消防栓
- Motorbike / 摩托車 / 機車
- Traffic light / 紅綠燈
- Vehicle / 機動車
- Bicycle / 自行車 / 腳踏車
- Train / 火車
- Parking meter / 停車收費表
- boat / 船

b. sub-chacha supported label (trained on re-capcha dataset):
- "拖拉機": "tractor"
- "棕櫚樹": "palmtree"
- "樓梯": "stairs"
- "橋樑": "bridge"
- "煙囪": "chimney"
- "行人穿越道":"zebracross"
- "雕像": "statue"
- "高山或山丘": "mountainhill"

### Run demo testing on grpc

- First, download the pretrained coco model [here](https://drive.google.com/file/d/1DHv5lKP6fjr2cIaznQQvKDJoDDSzQj9b/), and extract the content of zip to checkpoints folder.
- Second, download the sub-chacha model [here](https://drive.google.com/file/d/1mxcjLaVEzr7EYYGeOSTOARR-tmxP5Oce/), and extract the content of zip to checkpoints folder.

```
cd recaptcha_grpc
recaptcha_grpc$ python recaptcha_server.py

open another terminal
recaptcha_grpc$ python recaptcha_client.py
```

### Procedure for training the model

I will update later

### Obsolute

The following below is obsolute, I will update later.

Demo python script:

First, download the model [here](https://drive.google.com/file/d/1DHv5lKP6fjr2cIaznQQvKDJoDDSzQj9b/), and extract the content of zip to checkpoints folder.
Second, download the sample data [here](https://drive.google.com/file/d/11XWacc3iv4K67nGmRHkbH0OVD0Vg9IHP/), put the zip to folder data and extract it.
```
$python demo.py --image_folder chacha/data/labeled_captcha_1 --output_folder chacha/data/detected_captcha_1
```


### dataset
    https://drive.google.com/file/d/1IYyC_FKllJtxLsxEvn2MADLpJajKla8A/view?usp=sharing

    將資料解壓縮到
    ./rechaptcha/image-20200229T093045z-001/voc_format
### train
    再chacha目錄下執行

    python train.py --dataset '../rechaptcha/image-20200229T093045Z-001/voc_format/chacha_batch1_2.tfrecord' --val_dataset ../rechaptcha/image-20200229T093045Z-001/voc_format/batch1/chacha_batch1.tfrecord --classes ./data/subchacha.names --num_classes 8 --mode fit --transfer darknet --batch_size 16 --epochs 10 --weights ./checkpoints/yolov3.tf --weights_num_classes 80

    train完的data會在 ./checkpoints 裡面

### tool位置 如果要建立自己的database 請參database目錄建立 所需的檔案都可以用以下工具產生
./chacha/chacha/tools

產生tfrecord：voc2012.py
jason2xml : coco2voc.py








