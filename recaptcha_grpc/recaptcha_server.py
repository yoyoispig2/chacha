import sys
sys.path.append("..") # Adds higher directory to python modules path.

import json
from concurrent import futures
import time

import grpc

import cv2
import numpy as np

from absl import logging
import tensorflow as tf
from chacha.models import (
    YoloV3, YoloV3Tiny
)

from chacha.dataset import transform_images
from chacha.utils import draw_outputs

import os

import recaptcha_pb2
import recaptcha_pb2_grpc
from absl import app

os.environ['CUDA_VISIBLE_DEVICES'] = '1'

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class Greeter(recaptcha_pb2_grpc.RecaptchaServiceServicer):
    def __init__(self, classes, weight, size, is_tiny=False):
        self.classes = classes
        self.weight = weight
        self.is_tiny = is_tiny
        self.size = size

        self.map_class =json.load(open("class.txt"))
        physical_devices = tf.config.experimental.list_physical_devices('GPU')
        if len(physical_devices) > 0:
            tf.config.experimental.set_memory_growth(physical_devices[0], True)

        if self.is_tiny:
            self.yolo = YoloV3Tiny(classes=80)
        else:
            self.yolo = YoloV3(classes=80)

        self.yolo.load_weights(self.weight).expect_partial()
        logging.info('weights loaded')

        self.class_names = [c.strip() for c in open(self.classes).readlines()]
        logging.info('classes loaded')

    def Recognize(self, request, context):
        img_raw = tf.image.decode_image(request.bytes_data, channels=3)
        img = tf.expand_dims(img_raw, 0)
        img = transform_images(img, self.size)

        t1 = time.time()
        boxes, scores, classes, nums = self.yolo(img)
        t2 = time.time()
        logging.info('time: {}'.format(t2 - t1))

        logging.info('detections:')
        for i in range(nums[0]):
            logging.info('\t{}, {}, {}'.format(self.class_names[int(classes[0][i])],
                                               np.array(scores[0][i]),
                                               np.array(boxes[0][i])))

        img = cv2.cvtColor(img_raw.numpy(), cv2.COLOR_RGB2BGR)

        boxes, objectness, classes, nums = boxes[0], scores[0], classes[0], nums[0]
        wh = np.flip(img.shape[0:2])

        target_class = self.map_class[request.imageSrc]

        if target_class == 'vehicle':
            label_class = ['car', 'motorbike', 'bus', 'truck']
        elif target_class == 'taxi':
            label_class = ['car']
        else:
            label_class = [target_class]

        correct_detected = False

        results = []
        for i in range(nums):
            try:
                if self.class_names[int(classes[i])] in label_class:
                    x1y1 = tuple((np.array(boxes[i][0:2]) * wh).astype(np.int32))
                    x2y2 = tuple((np.array(boxes[i][2:4]) * wh).astype(np.int32))

                    img = cv2.rectangle(img, x1y1, x2y2, (255, 0, 0), 2)
                    img = cv2.putText(img, '{} {:.4f}'.format(
                        self.class_names[int(classes[i])], objectness[i]),
                                    x1y1, cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 2)
                    img = cv2.putText(img, '{} {:.4f}'.format(target_class, objectness[i]),
                                    x1y1, cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 2)
                    result = recaptcha_pb2.RecaptchaResult()
                    result.x = x1y1[0]
                    result.y = x1y1[1]
                    result.width = x2y2[0] - x1y1[0]
                    result.height = x2y2[1] - x1y1[1]
                    result.confidence = objectness[i]
                    results.append(result)
                    correct_detected = True
            except:
                pass

        if correct_detected:
            print(request.name)
            try:
                cv2.imwrite("./server_image/vis_" + request.name, img)
            except:
                pass
            logging.info('output saved to: {}'.format("vis_" + request.name))
        return recaptcha_pb2.RecaptchaReply(result = results)

def serve(_argv):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    recaptcha_pb2_grpc.add_RecaptchaServiceServicer_to_server(Greeter("../data/coco.names", "../checkpoints/yolov3.tf", 416, False), server)
    server.add_insecure_port('[::]:50051')
    server.start() 
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    try:
        app.run(serve)
    except SystemExit:
        pass
