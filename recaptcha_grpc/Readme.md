### 環境安裝
    python :

    https://www.anaconda.com/     (根據作業系統安裝對應版本)

### create 虛擬環境:
    conda create -n chacha python =3.7.4
    conda activate chacha
    pip install grpcio
    pip install opencv-pyll thon
    pip install absl-pya
    pip install tensorflow
    pip install matplotlib
    pip install Pillow
    pip install beautifulsoup4
    pip install selenium




### chromedrive :
    http://chromedriver.storage.googleapis.com/index.html(

    根據作業系統去抓取版本,放在recaptcha_grpc目錄下取代原本的檔案

### 執行server 
    再recaptcha_grpc下 打開terminal
    conda activate chacha
    python recaptcha_server.py

### 執行client
    再recaptcha_grpc下 打開terminal
    conda activate chacha
    python recaptcha_client.py