import grpc
import cv2
import recaptcha_pb2
import recaptcha_pb2_grpc
from recaptcha_pb2 import RecaptchaReply
import asyncio
from concurrent.futures.thread import ThreadPoolExecutor
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import warnings
import time
from random import randint
import os
import urllib
import datetime
import numpy as np
import json
import urllib.request

warnings.filterwarnings('ignore')
executor = ThreadPoolExecutor(1)
def img_position(x,y,width,height,img_w,img_h,size):
    x_list = []
    y_list = []
    for i in range(size):
        if i == 0:
            x_list.append(float(x))
            y_list.append(float(y))
        else:
            x_list.append(x+(width/size)*i)
            y_list.append(y+(height/size)*i)

    w_list = [img_w/size,img_w/size*2,float(img_w)]
    h_list = [img_h/size,img_h/size*2,float(img_h)]

    x_flag = []
    y_flag = []
    for x_ in x_list:
        count = 0
        for w_ in  w_list:
            if ( x_ < float(w_) ):
                x_flag.append(count)
                break
            count += 1
    for y_ in y_list:
        count = 0
        for w_ in  w_list:
            if ( y_ < float(w_) ):
                y_flag.append(count)
                break
            count += 1
    ans_ = [0]*size*size

    for x_ in x_flag:
        for y_ in y_flag:
            ans_[x_+size*y_] = 1
    return ans_


def scrape( *, loop , url ,ip):
    loop.run_in_executor(executor, recaptcha_test , url ,ip)
def recaptcha_test( url ,ip):

    map_class = json.load(open("all_class.txt"))
    # label_class = ['car', 'motorbike', 'bus', 'truck']

    #grpc_server ip
    channel = grpc.insecure_channel('localhost:50051')
    stub = recaptcha_pb2_grpc.RecaptchaServiceStub(channel)

    chromeOptions = webdriver.ChromeOptions()
    # chromeOptions.add_argument('--proxy-server='+ip) 
    chromeOptions.add_experimental_option('prefs', {'intl.accept_languages': 'zh-Hant-TW,zh-Hant-TW'})
    chromeOptions.add_argument("--incognito")
    driver = webdriver.Chrome('./chromedriver',chrome_options=chromeOptions)
    driver.implicitly_wait(5)
    driver.get(url)
    count = 0
    while(True):
        print(count)
        if ( count == 50):
            break
        try:
            images = driver.find_element_by_xpath('//img').get_attribute("src")
            titles = driver.find_element_by_xpath('//div/strong').text
            filepath = "./recaptcha/image/"+titles
            print(titles)
            if os.path.exists(filepath):
                file_len = len(os.listdir(filepath))
            else:
                os.mkdir(filepath)
                file_len = 0
            ts = str(int(datetime.datetime.now().timestamp()))
            urllib.request.urlretrieve(images, filepath+'/'+ts+'_'+str(file_len)+".jpg")
            # driver.find_element_by_xpath('//div[1]/form/input[3]').click()
            time.sleep(1)
            myfile = open(filepath+'/'+ts+'_'+str(file_len)+".jpg", 'rb')
            file_bytes = myfile.read()
            myfile.close()
            img = cv2.imread(filepath+'/'+ts+'_'+str(file_len)+".jpg") 
            name = ts+'_'+str(file_len)+".jpg"

            #check title in class
            try:
                print(map_class[titles])
            except:
                map_class.update({titles:'non'})
                json.dump(map_class, open("all_class.txt",'w', encoding="utf-8"),ensure_ascii=False)
            
            # 
            # if (map_class[titles] in label_class):
            response = stub.Recognize(recaptcha_pb2.RecaptchaRequest(name=name, imageSrc=titles, type=1, bytes_data=file_bytes))
            result = recaptcha_pb2.RecaptchaResult()
            Checkered = [0]*3*3
            for result in  response.result:
                Checkered = np.sum([Checkered,img_position(result.x,result.y,result.width,result.height,300,300,3)], axis = 0)
                img = cv2.rectangle(img, (result.x,result.y), (result.x+result.width,result.y+result.height), (255, 0, 0), 2)
                cv2.putText(img, '{} {:.4f}'.format(map_class[titles],result.confidence),(result.x,result.y), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 2)
            time.sleep(1)
            for i in range(len(Checkered)):
                if (Checkered[i] > 0):
                    Checkered[i] = 1
                else:
                    Checkered[i] = 0
            Checkered = [i for i in range(len(Checkered)) if Checkered[i] in [1]]
            for i in Checkered:
                driver.find_element_by_xpath('//div[1]/input['+str(i+1)+']').click()
                time.sleep(1)
            driver.find_element_by_xpath('//div[2]/input').click()

            time.sleep(3)
            if ( driver.find_element_by_xpath('//div[3]/div[1]').text == '複製這段程式碼並貼到下方空白框內。'):
                driver.get(url)
                if not os.path.isdir("client_image/succeed"):
                    os.makedirs("client_image/succeed")
                cv2.imwrite('./client_image/succeed/' + ts + '_' + str(file_len) + ".jpg", img)
            else:
                if not os.path.isdir("client_image/failed"):
                    os.makedirs("client_image/failed")
                cv2.imwrite('./client_image/failed/'+ts+'_'+str(file_len)+".jpg", img)
            # else:
            #     driver.find_element_by_xpath('//div[1]/form/input[3]').click()
            count+=1
        except Exception as e:
            print('error:' , e)
            driver.get(url)
            count+=1
    driver.quit() 
    
def run_feq():
    if os.path.exists('./recaptcha/image'):
        pass
    else:
        os.makedirs('./recaptcha/image')
        
    #load proxy 
    # fp = open('proxy.txt', "r")
    # line = fp.readline()
    proxy_ = []

    with open('proxy.txt', "r") as fp:
        for line in fp:
            proxy_.append(line.strip())
    # proxy_ = [
    #     '69.30.240.226:15001',
    #     '195.154.255.118:15001',
    #     '63.141.241.98:16001',
    #     '163.172.36.211:16001',
    #     '5.79.73.131:13010'
    # ]
    url_list =[
        'https://www.google.com/recaptcha/api/fallback?k=6LfBixYUAAAAABhdHynFUIMA_sa4s-XsJvnjtgB0',
        'https://www.google.com/recaptcha/api/fallback?k=6LfdsXUUAAAAAAC08912CUzfZc9gBJZOCHcRLXCo',
        'https://www.google.com/recaptcha/api/fallback?k=6LdWPh4TAAAAAAX8OdPoiqINDmD_vaBHbdmZdryS',
    ]
    map_class = json.load(open("all_class.txt"))
    #run
    loop = asyncio.get_event_loop()
    for i in range(1):
        ip = (proxy_[randint(0,len(proxy_)-1)])
        url = (url_list[randint(0,len(url_list)-1)])
        print(url, ip)
        scrape( loop=loop, url = url,ip=ip)

run_feq()