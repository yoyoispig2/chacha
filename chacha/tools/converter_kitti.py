from scipy import misc
from glob import glob
import json, sys, os
from os import listdir
from os.path import isfile, join
import cv2
import numpy as np
from PIL import ImageEnhance, Image
import ntpath

def get_enclosing_box(corners):
    """Get an enclosing box for ratated corners of a bounding box

    Parameters
    ----------

    corners : numpy.ndarray
        Numpy array of shape `N x 8` containing N bounding boxes each described by their
        corner co-ordinates `x1 y1 x2 y2 x3 y3 x4 y4`

    Returns
    -------

    numpy.ndarray
        Numpy array containing enclosing bounding boxes of shape `N X 4` where N is the
        number of bounding boxes and the bounding boxes are represented in the
        format `x1 y1 x2 y2`

    """
    x_ = corners[:, [0, 2, 4, 6]]
    y_ = corners[:, [1, 3, 5, 7]]

    xmin = np.min(x_, 1).reshape(-1, 1)
    ymin = np.min(y_, 1).reshape(-1, 1)
    xmax = np.max(x_, 1).reshape(-1, 1)
    ymax = np.max(y_, 1).reshape(-1, 1)

    final = np.hstack((xmin, ymin, xmax, ymax, corners[:, 8:]))

    return final

def rotate_box(corners, angle, cx, cy, h, w):
    """Rotate the bounding box.


    Parameters
    ----------

    corners : numpy.ndarray
        Numpy array of shape `N x 8` containing N bounding boxes each described by their
        corner co-ordinates `x1 y1 x2 y2 x3 y3 x4 y4`

    angle : float
        angle by which the image is to be rotated

    cx : int
        x coordinate of the center of image (about which the box will be rotated)

    cy : int
        y coordinate of the center of image (about which the box will be rotated)

    h : int
        height of the image

    w : int
        width of the image

    Returns
    -------

    numpy.ndarray
        Numpy array of shape `N x 8` containing N rotated bounding boxes each described by their
        corner co-ordinates `x1 y1 x2 y2 x3 y3 x4 y4`
    """

    corners = corners.reshape(-1, 2)
    corners = np.hstack((corners, np.ones((corners.shape[0], 1), dtype=type(corners[0][0]))))

    M = cv2.getRotationMatrix2D((cx, cy), angle, 1.0)

    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cx
    M[1, 2] += (nH / 2) - cy
    # Prepare the vector to be transformed
    calculated = np.dot(M, corners.T).T

    calculated = calculated.reshape(-1, 8)

    return calculated

def get_corners(bboxes):
    """Get corners of bounding boxes

    Parameters
    ----------

    bboxes: numpy.ndarray
        Numpy array containing bounding boxes of shape `N X 4` where N is the
        number of bounding boxes and the bounding boxes are represented in the
        format `x1 y1 x2 y2`

    returns
    -------

    numpy.ndarray
        Numpy array of shape `N x 8` containing N bounding boxes each described by their
        corner co-ordinates `x1 y1 x2 y2 x3 y3 x4 y4`

    """
    width = (bboxes[:, 2] - bboxes[:, 0]).reshape(-1, 1)
    height = (bboxes[:, 3] - bboxes[:, 1]).reshape(-1, 1)

    x1 = bboxes[:, 0].reshape(-1, 1)
    y1 = bboxes[:, 1].reshape(-1, 1)

    x2 = x1 + width
    y2 = y1

    x3 = x1
    y3 = y1 + height

    x4 = bboxes[:, 2].reshape(-1, 1)
    y4 = bboxes[:, 3].reshape(-1, 1)

    corners = np.hstack((x1, y1, x2, y2, x3, y3, x4, y4))

    return corners

def rotate_im(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))

def rotate_data(img, bboxes):

    angle = 90 #random.uniform(*self.angle)

    w,h = img.shape[1], img.shape[0]
    cx, cy = w//2, h//2

    img = rotate_im(img, angle)

    corners = get_corners(bboxes)

    corners = np.hstack((corners, bboxes[:,4:]))


    corners[:,:8] = rotate_box(corners[:,:8], angle, cx, cy, h, w)

    new_bbox = get_enclosing_box(corners)


    scale_factor_x = img.shape[1] / w

    scale_factor_y = img.shape[0] / h

    img = cv2.resize(img, (w,h))

    new_bbox[:,:4] /= [scale_factor_x, scale_factor_y, scale_factor_x, scale_factor_y]

    bboxes  = new_bbox

    # bboxes = clip_box(bboxes, [0,0,w, h], 0.25)

    return img, bboxes

# rotate = False
# if rotate:
#     img_width = 540  # 528 #640
#     img_height = 960  # 1072 #360\
#
# else:
#     img_width = 192#528 #640
#     img_height = 192#1072 #360\

kitti_root = "/media/siiva/DataStore/OUTPUT/rechaptcha/image-20200229T093045Z-001/kitti_format/batch2"
root_folder_images = '/media/siiva/DataStore/OUTPUT/rechaptcha/image-20200229T093045Z-001/for_training_image/batch2'
ignore_class = -1  # dont care
not_scale = True

coco_anno_path = '/media/siiva/DataStore/OUTPUT/rechaptcha/image-20200229T093045Z-001/for_training_json/batch2'
annotation_files = [f for f in listdir(coco_anno_path) if isfile(join(coco_anno_path, f))]

print("%d annotations files found in folder %s" % (len(annotation_files), annotation_files))

training_set = open(os.path.join(kitti_root, 'train.txt'), 'w')
# loading all the files and annotations
for anno_file_id in range(len(annotation_files)):
    jsonfilename = annotation_files[anno_file_id]
    annotype = jsonfilename.split('_')[0]

    if annotype == '拖拉機': #_tractor
        samples_class = 1
    elif annotype == '棕櫚樹': #_palmtree_chacha
        samples_class = 2
    elif annotype == '樓梯': #_stairs_chacha
        samples_class = 3
    elif annotype == '橋樑': #_bridge_chacha
        samples_class = 4
    elif annotype == '煙囪': #_chimney_chacha
        samples_class = 5
    elif annotype == '行人穿越道': #_zebracross_chacha
        samples_class = 6
    elif annotype == '雕像': #_statue_chacha
        samples_class = 7
    elif annotype == '高山或山丘': #_mountainhill_chacha
        samples_class = 8


    with open(coco_anno_path + '/' + annotation_files[anno_file_id]) as json_data:
        # get Json annotations
        print("Using annotation: %s" % (coco_anno_path + '/' + annotation_files[anno_file_id]))
        data = json.load(json_data)

        # locations where BBoxes are going to be saved
        bbox_x1 = []
        bbox_y1 = []
        bbox_x2 = []
        bbox_y2 = []
        classes_id = []

        bbox_x1_fake = []
        bbox_y1_fake = []
        bbox_x2_fake = []
        bbox_y2_fake = []
        classes_id_fake = []

        images_path = []

        # looping for all the files
        last_save = None
        annotations_counter = 0
        for ann_id in range(len(data['annotations'])):

            # get annotations and images
            img_id = data['annotations'][ann_id]['image_id']

            if len(data['images']) > img_id:
                image_path = data['images'][img_id]['file_name']
                img = cv2.imread(os.path.join(root_folder_images, annotype, image_path))

                # if rotate:
                #     img = rotate_bound(img, 90)

                height, width, _ = img.shape

                # if samples_class==1:
                #     rescale_x = 1.0 * 540 / 270#float(width)
                #     rescale_y = 1.0 * 960 / 480#float(height)
                # else:
                if not_scale:
                    img_width = width
                    img_height = height
                rescale_x = 1.0 #* img_width / float(width)
                rescale_y = 1.0 #* img_height / float(height)


                # getting bounding boxes
                # if samples_class==1:
                x1 = data['annotations'][ann_id]['bbox'][0] * rescale_x
                y1 = data['annotations'][ann_id]['bbox'][1] * rescale_y
                x2 = (data['annotations'][ann_id]['bbox'][0] + data['annotations'][ann_id]['bbox'][2]) * rescale_x
                y2 = (data['annotations'][ann_id]['bbox'][1] + data['annotations'][ann_id]['bbox'][3]) * rescale_y
                # else:
                #     x1 = data['annotations'][ann_id]['bbox'][0] * rescale_x
                #     y1 = data['annotations'][ann_id]['bbox'][1] * rescale_y + 160
                #     x2 = (data['annotations'][ann_id]['bbox'][0] + data['annotations'][ann_id]['bbox'][2]) * rescale_x
                #     y2 = (data['annotations'][ann_id]['bbox'][1] + data['annotations'][ann_id]['bbox'][3]) * rescale_y + 160

                # check all annotations are valid
                if x1 < 0 or y1 < 0 or x2 >= img_width or y2 >= img_height or x1 >= x2 - 5 or y1 >= y2 - 5:
                    # this BBox is illegal
                    print(x1, y1, x2, y2)
                    print("ERROR ---> This annotation is illegal")
                    continue

                annotations_counter += 1

                images_path.append(image_path)
                classes_id.append(samples_class)  # data['annotations'][ann_id]['category_id']

                # real annotation
                bbox_x1.append(x1)  # left top x
                bbox_y1.append(y1)  # left top y
                bbox_x2.append(x2)  # right bottom x
                bbox_y2.append(y2)  # right bottom y

                print("Image: %s\n "
                      "BBox extracted was (REAL): [%d, %d, %d %d] [w: %d, h: %d] [class: %d]" %
                      (images_path[len(images_path) - 1], bbox_x1[len(bbox_x1) - 1], bbox_y1[len(bbox_y1) - 1],
                       bbox_x2[len(bbox_x2) - 1], bbox_y2[len(bbox_y2) - 1],
                       data['annotations'][ann_id]['bbox'][2], data['annotations'][ann_id]['bbox'][3],
                       classes_id[len(classes_id) - 1]))


            else:
                print("Image ID not found: %d requested of %d available" % (img_id, len(data['images'])))
                continue



            if ignore_class == data['annotations'][ann_id]['category_id']:
                print("ignoring this class ... %d" % data['annotations'][ann_id]['category_id'])
                continue



        print("Data conversions is summarized as follows:\n"
              "N_images: %d\n"
              "N_annotations: [%d/%d]\n"
              "Resolutions: %dx%d\n" % (len(data['images']), len(bbox_x1), annotations_counter, rescale_y * img_height,
                                        rescale_x * img_width))

    # =====================================================================================================================
    '''
        format is as follows:

        1 type Describes the type of object: '00normal', '01dont_care', '02black', '03sour', '04shell',
        '05fungus', '06insect', '07unknow'

        1 truncated Float from 0 (non-truncated) to 1 (truncated), where truncated refers to the object leaving image boundaries

        1 occluded Integer (0,1,2,3) indicating occlusion state:

        0 = fully visible, 1 = partly occluded

        4 bbox 2D bounding box of object in the image (0-based index): contains left, top, right, bottom pixel coordinates

        3 dimensions 3D object dimensions: height, width, length (in meters)

        3 location 3D object location x,y,z in camera coordinates (in meters)

        1 rotation_y Rotation ry around Y-axis in camera coordinates [-pi..pi]

        1 score Only for results: Float, indicating confidence in detection, needed for p/r curves, higher is better.

        2 = largely occluded, 3 = unknown

        1 alpha Observation angle of object, ranging [-pi..pi]
    '''

    if annotype == '拖拉機':  # _tractor
        samples_class = 1
    elif annotype == '棕櫚樹':  # _palmtree_chacha
        samples_class = 2
    elif annotype == '樓梯':  # _stairs_chacha
        samples_class = 3
    elif annotype == '橋樑':  # _bridge_chacha
        samples_class = 4
    elif annotype == '煙囪':  # _chimney_chacha
        samples_class = 5
    elif annotype == '行人穿越道':  # _zebracross_chacha
        samples_class = 6
    elif annotype == '雕像':  # _statue_chacha
        samples_class = 7
    elif annotype == '高山或山丘':  # _mountainhill_chacha
        samples_class = 8

    class_name = ['dummy', 'tractor', 'palmtree', 'stairs', 'bridge', 'chimney', 'zebracross', 'statue', 'mountainhill']
    last_save = None
    truncated = 0.0
    ocluded = 0.0
    fully_visible = 0.0
    large_ocluded = 0.0
    alpha = 0.0
    general = 0.0

    n_fake_sample_id = 0

    player_id = 1
    for anno_id in range(annotations_counter):
        file_name_combined = images_path[anno_id].split('/')
        file_name_single_ext = file_name_combined[len(file_name_combined) - 1]
        # print(file_name_single_ext)

        file_name = file_name_single_ext[:-4]#file_name_single_ext.split('.')[0]

        annotation_file = open(os.path.join(kitti_root, "training/label_2", file_name + '_' + class_name[classes_id[anno_id]] +'.txt'), 'a')

        print("USING ANNOTATION FILE (%d): %s" % (anno_id, ("training/label_2"+'/'+file_name + '_' + class_name[classes_id[anno_id]] +'.txt')))

        # print(class_name[classes_id[anno_id]])

        # writing to file REAL Bboxes
        annotation_file.write(
            class_name[classes_id[anno_id]] + ' ' +
            str(truncated) + ' ' +
            str(ocluded) + ' ' +
            str(fully_visible) + ' ' +
            str(bbox_x1[anno_id]) + ' ' +
            str(bbox_y1[anno_id]) + ' ' +
            str(bbox_x2[anno_id]) + ' ' +
            str(bbox_y2[anno_id]) + ' ' +
            str(general) + ' ' +
            str(general) + ' ' +
            str(general) + ' ' +
            str(general) + ' ' +
            str(general) + ' ' +
            str(general) + ' ' +
            str(general)
        )
        annotation_file.write('\n')

        # flushing everything
        annotation_file.flush()
        annotation_file.close()

        if last_save is not images_path[anno_id]:
            filename2save = os.path.join(kitti_root, "training/image_2", file_name + '_' + class_name[classes_id[anno_id]] +'.jpg')
            if os.path.exists(filename2save):
                pass
            else:
                last_save = os.path.join(root_folder_images, annotype, images_path[anno_id])

                if not os.path.exists(last_save):
                    continue

                #img = misc.imread(last_save, mode='RGB')
                img = cv2.imread(last_save)
                cv2.imwrite(filename2save, img)
                #img = misc.imresize(img, [img_height, img_width])
                #misc.imsave(image_files_path+'/'+file_name+'.jpg', img)

                training_set.write(file_name + '_' + class_name[classes_id[anno_id]]+'\n')
                training_set.flush()
                print("annotation on file %s, done" % last_save)

                # img = cv2.imread(last_save, -1)
                # cv2.imwrite(image_files_path_ori+'/'+file_name+'.png', img)

            # # ===========================================================
            # # Color Image
            # # ===========================================================
            # #
            # for idx in range(3):
            #     file_name_combined = images_path[anno_id].split('/')
            #     file_name_single_ext = file_name_combined[len(file_name_combined) - 1]
            #     file_name = file_name_single_ext.split('.')[0] + ("_color_%d" % idx)
            #
            #     annotation_file = open(labels_files_path + '/' + file_name + '.txt', 'a')
            #
            #     # writing to file REAL Bboxes
            #     annotation_file.write(
            #         class_name[classes_id[anno_id]] + ' ' +
            #         str(truncated) + ' ' +
            #         str(ocluded) + ' ' +
            #         str(fully_visible) + ' ' +
            #         str(bbox_x1[anno_id]) + ' ' +
            #         str(bbox_y1[anno_id]) + ' ' +
            #         str(bbox_x2[anno_id]) + ' ' +
            #         str(bbox_y2[anno_id]) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general)
            #     )
            #     annotation_file.write('\n')
            #
            #     # flushing everything
            #     annotation_file.flush()
            #     annotation_file.close()
            #
            #     if last_save is not images_path[anno_id]:
            #         filename2save = image_files_path + '/' + file_name + '.jpg'
            #         if os.path.exists(filename2save):
            #             pass
            #         else:
            #             last_save = root_folder_images + '/' + images_path[anno_id]
            #
            #             if not os.path.exists(last_save):
            #                 continue
            #
            #             img = misc.imread(last_save, mode='RGB')
            #
            #             pil_image = Image.fromarray(img, "RGB")
            #             colorer = ImageEnhance.Color(pil_image)
            #             factor = 0.3 * (idx + 1)
            #             colored_image = colorer.enhance(factor)
            #             img = np.asarray(colored_image)
            #
            #             misc.imsave(image_files_path + '/' + file_name + '.jpg', img)
            #             training_set.write(file_name + '\n')
            #             training_set.flush()
            #             print("annotation on file %s, done" % last_save)
            #
            # #
            # # ===========================================================
            # # Brightness Image
            # # ===========================================================
            # #
            # for idx in range(3):
            #     file_name_combined = images_path[anno_id].split('/')
            #     file_name_single_ext = file_name_combined[len(file_name_combined) - 1]
            #     file_name = file_name_single_ext.split('.')[0] + ("_brightness_%d" % idx)
            #
            #     annotation_file = open(labels_files_path + '/' + file_name + '.txt', 'a')
            #
            #     # writing to file REAL Bboxes
            #     annotation_file.write(
            #         class_name[classes_id[anno_id]] + ' ' +
            #         str(truncated) + ' ' +
            #         str(ocluded) + ' ' +
            #         str(fully_visible) + ' ' +
            #         str(bbox_x1[anno_id]) + ' ' +
            #         str(bbox_y1[anno_id]) + ' ' +
            #         str(bbox_x2[anno_id]) + ' ' +
            #         str(bbox_y2[anno_id]) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general)
            #     )
            #     annotation_file.write('\n')
            #
            #     # flushing everything
            #     annotation_file.flush()
            #     annotation_file.close()
            #
            #     if last_save is not images_path[anno_id]:
            #         filename2save = image_files_path + '/' + file_name + '.jpg'
            #
            #     if os.path.exists(filename2save):
            #         pass
            #     else:
            #         last_save = root_folder_images + '/' + images_path[anno_id]
            #
            #         if not os.path.exists(last_save):
            #             continue
            #
            #         img = misc.imread(last_save, mode='RGB')
            #
            #         pil_image = Image.fromarray(img, "RGB")
            #         brighter = ImageEnhance.Brightness(pil_image)
            #         factor = 0.3 * (idx + 1)
            #         bright_image = brighter.enhance(factor)
            #         img = np.asarray(bright_image)
            #
            #         misc.imsave(image_files_path + '/' + file_name + '.jpg', img)
            #         training_set.write(file_name + '\n')
            #         training_set.flush()
            #         print("annotation on file %s, done" % last_save)
            #
            # #
            # # ===========================================================
            # # Contrast Image
            # # ===========================================================
            # #
            # for idx in range(3):
            #     file_name_combined = images_path[anno_id].split('/')
            #     file_name_single_ext = file_name_combined[len(file_name_combined) - 1]
            #     file_name = file_name_single_ext.split('.')[0] + ("_contrast_%d" % idx)
            #
            #     annotation_file = open(labels_files_path + '/' + file_name + '.txt', 'a')
            #
            #     # writing to file REAL Bboxes
            #     annotation_file.write(
            #         class_name[classes_id[anno_id]] + ' ' +
            #         str(truncated) + ' ' +
            #         str(ocluded) + ' ' +
            #         str(fully_visible) + ' ' +
            #         str(bbox_x1[anno_id]) + ' ' +
            #         str(bbox_y1[anno_id]) + ' ' +
            #         str(bbox_x2[anno_id]) + ' ' +
            #         str(bbox_y2[anno_id]) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general)
            #     )
            #     annotation_file.write('\n')
            #
            #     # flushing everything
            #     annotation_file.flush()
            #     annotation_file.close()
            #
            #     if last_save is not images_path[anno_id]:
            #         filename2save = image_files_path + '/' + file_name + '.jpg'
            #
            #     if os.path.exists(filename2save):
            #         pass
            #     else:
            #         last_save = root_folder_images + '/' + images_path[anno_id]
            #
            #         if not os.path.exists(last_save):
            #             continue
            #
            #         img = misc.imread(last_save, mode='RGB')
            #
            #         pil_image = Image.fromarray(img, "RGB")
            #         contraster = ImageEnhance.Contrast(pil_image)
            #         factor = 0.3 * (idx + 1)
            #         contrasted_image = contraster.enhance(factor)
            #         img = np.asarray(contrasted_image)
            #
            #         misc.imsave(image_files_path + '/' + file_name + '.jpg', img)
            #         training_set.write(file_name + '\n')
            #         training_set.flush()
            #         print("annotation on file %s, done" % last_save)
            #
            # #
            # # ===========================================================
            # # BLurring Image
            # # ===========================================================
            # #
            # for idx in range(5):
            #     file_name_combined = images_path[anno_id].split('/')
            #     file_name_single_ext = file_name_combined[len(file_name_combined) - 1]
            #     file_name = file_name_single_ext.split('.')[0] + ("_sharp_%d" % idx)
            #
            #     annotation_file = open(labels_files_path + '/' + file_name + '.txt', 'a')
            #
            #     # writing to file REAL Bboxes
            #     annotation_file.write(
            #         class_name[classes_id[anno_id]] + ' ' +
            #         str(truncated) + ' ' +
            #         str(ocluded) + ' ' +
            #         str(fully_visible) + ' ' +
            #         str(bbox_x1[anno_id]) + ' ' +
            #         str(bbox_y1[anno_id]) + ' ' +
            #         str(bbox_x2[anno_id]) + ' ' +
            #         str(bbox_y2[anno_id]) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general) + ' ' +
            #         str(general)
            #     )
            #     annotation_file.write('\n')
            #
            #     # flushing everything
            #     annotation_file.flush()
            #     annotation_file.close()
            #
            #     if last_save is not images_path[anno_id]:
            #         filename2save = image_files_path + '/' + file_name + '.jpg'
            #
            #     if os.path.exists(filename2save):
            #         pass
            #     else:
            #         last_save = root_folder_images + '/' + images_path[anno_id]
            #
            #         if not os.path.exists(last_save):
            #             continue
            #
            #         img = misc.imread(last_save, mode='RGB')
            #
            #         pil_image = Image.fromarray(img, "RGB")
            #         blurrer = ImageEnhance.Sharpness(pil_image)
            #         factor = 0.4 * (idx + 1)
            #         blurred_image = blurrer.enhance(factor)
            #         img = np.asarray(blurred_image)
            #
            #         misc.imsave(image_files_path + '/' + file_name + '.jpg', img)
            #         training_set.write(file_name + '\n')
            #         training_set.flush()
            #         print("annotation on file %s, done" % last_save)

training_set.close()

if True:
    image_files = [f for f in listdir(join(kitti_root, "training/image_2")) if isfile(join(kitti_root, "training/image_2", f))]

    for im_id in range(len(image_files)):
        image_basename = image_files[im_id][:-4]#.split('.')[0]
        annotation_image = join(kitti_root, "training/label_2", image_basename +'.txt')

        global_counter_here = 0

        with open(annotation_image) as f:
            content = f.readlines()
            content = [x.strip() for x in content]

            global_counter_here += 1
            if global_counter_here == 1:
                # img = misc.imread(image_files_path+'/'+image_files[im_id], mode='RGB')
                # img = misc.imresize(img, [img_height, img_width])

                img = cv2.imread(join(kitti_root, "training/image_2",image_files[im_id]))

            elif global_counter_here > 1:
                # img = misc.imread(debug_images_path+'/'+image_basename+'.jpg', mode='RGB')
                img = cv2.imread(join(kitti_root, "training/image_2",image_files[im_id]))

                # img = misc.imresize(img, [img_height, img_width])
            for ann_idx in range(len(content)):
                # plus the real one
                content_intern = content[ann_idx].split(' ')
                print(content_intern)
                class_name = content_intern[0]
                content_intern = np.array(content_intern[1:]).astype(np.float)
                print(class_name, content_intern, content_intern.shape)

                cv2.rectangle(img, (int(content_intern[3]), int(content_intern[4])),
                    (int(content_intern[5]), int(content_intern[6])), (51,255,255), 2)

                cv2.putText(img, class_name, (int(content_intern[3]+20), int(content_intern[4]+20)),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (51,255,51), 2)

                #closing image
                # misc.imsave(debug_images_path+'/'+image_basename+'.jpg', img)
                cv2.imwrite(join(kitti_root, 'debug_images', image_basename +'.jpg'), img)

# sys.exit(0)