import time
from absl import app, flags, logging
from absl.flags import FLAGS
import cv2
import numpy as np
import tensorflow as tf
from chacha.models import (
    YoloV3, YoloV3Tiny
)
from chacha.dataset import transform_images
from chacha.utils import draw_outputs

import os
from glob import glob
import ntpath
import mapping
import shutil

flags.DEFINE_string('classes', './data/coco.names', 'path to classes file')
flags.DEFINE_string('weights', './checkpoints/yolov3.tf',
                    'path to weights file')
flags.DEFINE_boolean('tiny', False, 'yolov3 or yolov3-tiny')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_string('image_folder', './data/labeled_captcha_1/', 'path to input image folder')
flags.DEFINE_string('output_folder', './data/detected_captcha_1/', 'path to output image folder')

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

def main(_argv):
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    if len(physical_devices) > 0:
        tf.config.experimental.set_memory_growth(physical_devices[0], True)

    if FLAGS.tiny:
        yolo = YoloV3Tiny(classes=80)
    else:
        yolo = YoloV3(classes=80)

    yolo.load_weights(FLAGS.weights).expect_partial()
    logging.info('weights loaded')

    class_names = [c.strip() for c in open(FLAGS.classes).readlines()]
    logging.info('classes loaded')

    folders = os.listdir(FLAGS.image_folder)
    for map_class in folders:
        if map_class not in mapping.CLASS.keys():
            continue
        target_class = mapping.CLASS[map_class]

        logging.info('try to detect classes-%s/%s' % (target_class, map_class))
        files = glob(os.path.join(FLAGS.image_folder, map_class, '*.jpg'))
        for file_ in files:
            img_raw = tf.image.decode_image(
                open(file_, 'rb').read(), channels=3)

            img = tf.expand_dims(img_raw, 0)
            img = transform_images(img, FLAGS.size)

            t1 = time.time()
            boxes, scores, classes, nums = yolo(img)
            t2 = time.time()
            logging.info('time: {}'.format(t2 - t1))

            logging.info('detections:')
            for i in range(nums[0]):
                logging.info('\t{}, {}, {}'.format(class_names[int(classes[0][i])],
                                                   np.array(scores[0][i]),
                                                   np.array(boxes[0][i])))

            img = cv2.cvtColor(img_raw.numpy(), cv2.COLOR_RGB2BGR)

            img, correct_detected = draw_outputs(img, (boxes, scores, classes, nums), class_names, target_class)

            if not os.path.isdir(os.path.join(FLAGS.output_folder, map_class)):
                os.makedirs(os.path.join(FLAGS.output_folder, map_class))

            if correct_detected:
                cv2.imwrite(os.path.join(FLAGS.output_folder, map_class, "vis_" + ntpath.basename(file_)), img)

                logging.info('output saved to: {}'.format(os.path.join(FLAGS.output_folder, map_class, "vis_" + ntpath.basename(file_))))
                shutil.move(os.path.join(FLAGS.image_folder, map_class, ntpath.basename(file_)),
                            os.path.join(FLAGS.output_folder, map_class, ntpath.basename(file_)))

if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
